var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var fs = require('fs');

app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
    console.log('connection: ' + socket.id);

    socket.extra = {};
    socket.extra.mouse = {};
    socket.extra.mouse.x;
    socket.extra.mouse.y;
    socket.extra.mouse.d;
    socket.extra.mouse.c;
    socket.extra.screen = {};
    socket.extra.screen.x;
    socket.extra.screen.y;
    socket.extra.screen.w;
    socket.extra.screen.h;

    socket.emit('id', socket.id);
    socket.emit('sockets', Object.keys(io.sockets.connected));
    for (var i in io.sockets.connected) {
        if (i !== socket.id) {
            var obj = {};
            obj.id = i;
            obj.parameters = io.sockets.connected[i].extra;
            socket.emit('socket', obj);
        }
    }

    socket.broadcast.emit('conn', socket.id);

    // on

    socket.on('disconnect', function(){
        console.log('disconnect: ' + socket.id);

        io.emit('disc', socket.id);
    });
    
    socket.on('socket', function(obj) {
        io.sockets.connected[socket.id].extra = obj; // all extra or ?

        var obj = {};
        obj.id = socket.id;
        obj.parameters = io.sockets.connected[i].extra;
        socket.broadcast.emit('socket', obj);
    });
    
    //fs.readFile('data.json', 'utf8', function (err, data) {
    //    if (err) return console.log(err);
    //    var objs = JSON.parse(data);
    //    socket.emit('add', objs);
    //});
    //// receive the new obj, add to data.json and send it to all clients
    //socket.on('add', function(obj){
    //    obj.item.date = new Date();
    //    obj.user.id = socket.id;

    //    fs.readFile('data.json', 'utf8', function (err, data) {
    //        if (err) return console.log(err);
    //        var objs = JSON.parse(data);
    //        objs.push(obj);
    //        fs.writeFile("data.json", JSON.stringify(objs, null, 4), function(err) {
    //            if (err) return console.log(err); 
    //            console.log("obj added");
    //            var objs = [];
    //            objs.push(obj);
    //            io.emit('add', objs); // only the new obj but in an array so we handle it with the same update function
    //        }); 
    //    });
    //});
   

});

http.listen(3000, function(){
    console.log('listening on *:3000');
});
